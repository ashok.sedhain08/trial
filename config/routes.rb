Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :books
  resources :authors
  resources :categories
  resources :publication_houses
  resources :sales
  root to: "books#index"
end
