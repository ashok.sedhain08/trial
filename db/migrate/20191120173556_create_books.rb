class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :name
      t.string :publication
      t.float :price
      t.datetime :publication_date
      t.string :author
      t.string :description
      t.timestamps
    end
  end
end
