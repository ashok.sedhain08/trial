class CreatePublicationHouses < ActiveRecord::Migration[6.0]
  def change
    create_table :publication_houses do |t|
      t.string :name
      t.string :reg_no
      t.string :address
      t.string :phone_no

      t.timestamps
    end
  end
end
