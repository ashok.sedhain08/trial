class RemovePublicationFromBook < ActiveRecord::Migration[6.0]
  def change

    remove_column :books, :publication, :string
  end
end
