class AddBookRefToSales < ActiveRecord::Migration[6.0]
  def change
    add_reference :sales, :book, foreign_key: true
  end
end
