class CreateSales < ActiveRecord::Migration[6.0]
  def change
    create_table :sales do |t|
      t.float :quantity
      t.float :amount

      t.timestamps
    end
  end
end
