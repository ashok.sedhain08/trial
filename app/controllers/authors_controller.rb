class AuthorsController < ApplicationController
  def index
    @authors=Author.all
  end

  def new
    @author = Author.new
    @publication_houses = PublicationHouse.all
  end

  def create
    binding.pry
    @author = Author.new author_params
    if @author.save
      redirect_to authors_path
    else
      @publication_houses = PublicationHouse.all
      render :new
    end
  end

  def edit
  end

  def update
    if @author.update author_params 
      redirect_to authors_path
    else
      render :edit
    end
  end
  def show
  end

  def destroy
    if @author.destroy 
      redirect_to authors_path
    else
      flash[:error]=@book.error.full_messages.join(", ")
      render :show
    end
  end

  private

  def author_params
    params.require(:author).permit(:name, :pen_name, :phone_no, :publication_house_id)
  end

  def permitted_params
    params.permit(:id)
  end

  def find_author
    @author=Author.find permitted_params["id"]
  end

end
