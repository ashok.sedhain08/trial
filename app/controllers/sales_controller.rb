class SalesController < ApplicationController
  def index
    @sales = Sale.all
  end

  def new
    @sale = Sale.new
  end

  def create
  end

  def edit
  end 

  def update
  end

  def show
  end

  def destroy
  end

  private

  def sale_params
  end

  def find_sale
  end

  def permitted_params
  end
  
end
