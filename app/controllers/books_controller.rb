class BooksController < ApplicationController
  def index
   @books=Book.all  
  end

  def new
    @book = Book.new
    @authors = Author.all
    @categories= Category.all
  end

  def create
    binding.pry
    @book = book.new book_params
    if @book.save
      redirect_to books_path
    else
      render :new
    end
  end

  def edit
    
  end 

  def update
    if @book.update book_params 
      redirect_to books_path
    else
      flash[error]=@book.error.full_messages.join(", ")
      render :edit
    end
  end

  def show
  end

  def destroy
    if @book.destroy 
      redirect_to books_path
    else
      render :show
    end
  end

  private

  def book_params
    params.require(:book).permit(:name)
  end

  def find_book
     @book=Book.find permitted_params["id"]
  end

  def permitted_params
    params.permit(:id)
  end
  
end
