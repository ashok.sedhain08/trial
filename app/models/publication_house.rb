class PublicationHouse < ApplicationRecord
  has_many :authors, dependent: :destroy
  has_many :books,   through: :authors
end
