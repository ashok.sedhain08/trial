class Author < ApplicationRecord
  has_many :books, dependent: :destroy
  belongs_to :publication_house

  validates :name, presence: true
end
