class Category < ApplicationRecord
  has_one :book, dependent: :destroy
  validates :name, presence: true
end
