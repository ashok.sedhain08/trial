class Book < ApplicationRecord
  belongs_to :author
  has_many :sales, dependent: :destroy
  belongs_to :publication_house
  belongs_to :category
end
